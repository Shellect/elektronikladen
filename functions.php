<?php
  function create_component($tag_name, $style_class, $content){
    return "<{$tag_name} class=\"{$style_class}\">{$content}</{$tag_name}>";
  }

  function navbar_widget(){
    $auth_links = '<li class="nav-item"><a href="/index.php?page=profile" class="nav-link">Profile</a></li>'
    . '<li class="nav-item"><a href="/index.php?page=logout" class="nav-link">Logout</a></li>';

    $guest_links = '<li class="nav-item"><a href="#" class="nav-link" data-bs-toggle="modal" data-bs-target="#signInModal">Sign In</a></li>'
    . '<li class="nav-item"><a href="#" class="nav-link" data-bs-toggle="modal" data-bs-target="#signUpModal">Sign Up</a></li>';

    $auth = $_SESSION['auth']??false;
    $links = $auth ? $auth_links : $guest_links;
    echo <<<HTML
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item"><a href="index.php?page=main" class="nav-link">Home</a></li>
              <li class="nav-item"><a href="index.php?page=catalog" class="nav-link">Catalog</a></li>
              <li class="nav-item"><a href="index.php?page=cart" class="nav-link">Cart</a></li>
              {$links}
            </ul>
            <form class="d-flex">
              <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
          </div>
        </div>
      </nav>
HTML;
  }
