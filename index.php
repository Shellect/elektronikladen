<?php
  require_once "functions.php";

  $page = $_GET['page']??'';
  $content = '';
  if ($page && file_exists('views/' . $page . '.php')){
    ob_start();
    include 'views/' . $page . '.php';
    $content = ob_get_clean();

  }
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>IT Step</title>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/master.css">
  </head>
  <body>
    <?php
      navbar_widget();
    ?>
    <div class="container">
    <?php
      echo $content;
    ?>
    </div>

    <div class="modal fade" tabindex="-1" id="signInModal" aria-labelledby="ModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Sign In</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form>
              <div class="mb-3">
                <label for="InputEmail1" class="form-label">Email address</label>
                <input type="email" class="form-control" id="InputEmail1" aria-describedby="emailHelp">
                <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
              </div>
              <div class="mb-3">
                <label for="InputPassword1" class="form-label">Password</label>
                <input type="password" class="form-control" id="InputPassword1">
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Sign In</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" tabindex="-1" id="signUpModal" aria-labelledby="ModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Sign Up</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form id="signUpForm">
              <div class="mb-3">
                <label for="signUpEmail" class="form-label">Email address: </label>
                <input type="email" class="form-control" id="signUpEmail" aria-describedby="emailHelp" name="email">
                <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
              </div>
              <div class="mb-3">
                <label for="signUplogin" class="form-label">Login: </label>
                <input type="text" class="form-control" id="signUplogin" name="login">
              </div>
              <div class="mb-3">
                <label for="signUpPass" class="form-label">Password: </label>
                <input type="password" class="form-control" id="signUpPass" name="pass">
              </div>
              <div class="mb-3">
                <label for="signUpPassConfirm" class="form-label">Password Confirm: </label>
                <input type="password" class="form-control" id="signUpPassConfirm" name="confirmPass">
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="signUpButton">Sign Up</button>
          </div>
        </div>
      </div>
    </div>

    <script src="node_modules/jquery/dist/jquery.min.js" charset="utf-8"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js" charset="utf-8"></script>
    <script src="assets/js/main.js" charset="utf-8"></script>
  </body>
</html>
